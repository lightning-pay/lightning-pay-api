[![pipeline status](https://gitlab.com/lightning-pay/lightning-pay-api/badges/master/pipeline.svg)](https://gitlab.com/lightning-pay/lightning-pay-api/commits/master)
[![coverage report](https://gitlab.com/lightning-pay/lightning-pay-api/badges/master/coverage.svg)](https://gitlab.com/lightning-pay/lightning-pay-api/commits/master)

# Lightning Pay API

This is a payment gateway for Lightning Network, made for retail POS integration.

Unlike many Lightning Network payment solutions, this service is meant to be operated on behalf of one or more merchants, as a SaaS platform.

# Running a single machine instance with docker-compose

### Start bitcoind and let it sync

To start the bitcoind for the first time, run the following command and watch the output to monitor the progress. *NOTE: This may take several days*

```sh
docker-compose up bitcoind
```

Once it has fully synchronized, you can shutdown the process with ctrl+c, and restart it as a background daemon with this command

```sh
docker-compose up -d bitcoind
```

### Start LND and create a wallet

First, start LND in the foreground to monitor the output

```sh
docker-compose up lndbtc
```

On the first run, the system will generate TLS certs. You must then create a wallet. Open a 2nd terminal, and use the following command, and follow the prompts to set a password.

```sh
docker-compose exec lndbtc lncli create
```

After successfully creating the wallet, the LND system will generate macaroons for securing gRPC access, and scan the blockchain for wallet data.

Next the LND system will begin the bootstrap process, finding other nodes to connect to.

If the system is not able to retrieve a list of peers to bootstrap, try setting your DNS servers to 8.8.8.8 and 8.8.4.4 (google). Many DNS providers will timeout trying to send the list (For example, 1.1.1.1 times out)

If the system is not able to bootstrap automatically, you can manually specify a few nodes to get it started. See http://lndhub.io for a fresh list. Below is an example:

```sh
docker-compose exec lndbtc lncli connect 03021c5f5f57322740e4ee6936452add19dc7ea7ccf90635f95119ab82a62ae268@207.180.244.165:9735
docker-compose exec lndbtc lncli connect 024a2e265cd66066b78a788ae615acdc84b5b0dec9efac36d7ac87513015eaf6ed@52.16.240.222:9735
docker-compose exec lndbtc lncli connect 02ad6fb8d693dc1e4569bcedefadf5f72a931ae027dc0f0c544b34c1c6f3b9a02b@167.99.50.31:9735
```

### Start MongoDB

Why MongoDB? Well, because MongoDB is free to use, has amazing high-availability and auto-failover features, and as of v4, has transactions too. It is very well suited for the job.

```sh
docker-compose up -d mongo
```

On the first start, you will need to enable replication.

```sh
docker-compose exec mongo mongo
> rs.initiate();
> exit
```

### Start Redis

Redis is used to enable clustered socket.io, as well as Kue, a work queue.

```sh
docker-compose up -d redis
```
### Start lightning-pay-worker

The worker process performs work queue tasks and connects directly to LND. You may run more than one of these in production for redundancy.

```sh
docker-compose up --build --no-deps lightning-pay-worker
```

### Start lightning-pay-api

The API process provides a REST API and socket.io, and does not connect directly to LND. You may run more than one of these in production for redundancy.

```sh
docker-compose up --build --no-deps lightning-pay-api
```

### lightning-pay-ui

The UI is a React app, and is provided seperately at https://gitlab.com/lightning-pay/lightning-pay-ui