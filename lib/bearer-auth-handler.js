const Payee = require('../models/payee');

module.exports = {
    validate: async (request, token) => {
        const query = {
            tokens: {
                $elemMatch: {
                    token,
                    expires: {$gt: new Date()}
                }
            }
        };

        const entity = await Payee.findOne(query);
        const isValid = !!entity;

        const credentials = {token};
        const artifacts = {};

        if (isValid) {
            artifacts.id = entity._id.toString();
            artifacts.name = entity.name;
        }

        return {isValid, credentials, artifacts};
    }
};