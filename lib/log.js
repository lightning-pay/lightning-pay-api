const config = require('config');

class Logger {
    constructor(options = {}, name) {
        this.name = name;
        this.level = options.level;
        this.levels = {
            none: 0,
            error: 1,
            warn: 2,
            info: 3,
            debug: 4
        };
        this.timestamp = options.timestamp || false;
    }

    _log() {
        if (this.name) {
            const args = Array.from(arguments);
            args.unshift((new Date()).toISOString());
            return console.log.apply(this, args);
        }
        if (this.timestamp) {
            const args = Array.from(arguments);
            args.unshift((new Date()).toISOString());
            return console.log.apply(this, args);
        }
        console.log.apply(this, arguments);
    }

    error() {
        const level = 'error';
        if (this.levels[this.level] >= this.levels[level]) {
            const args = Array.from(arguments);
            args.unshift(level);
            this._log.apply(this, args);
        }
    }

    warn() {
        const level = 'warn';
        if (this.levels[this.level] >= this.levels[level]) {
            const args = Array.from(arguments);
            args.unshift(`${level} `);
            this._log.apply(this, args);
        }
    }

    info() {
        const level = 'info';
        if (this.levels[this.level] >= this.levels[level]) {
            const args = Array.from(arguments);
            args.unshift(`${level} `);
            this._log.apply(this, args);
        }
    }

    debug() {
        const level = 'debug';
        if (this.levels[this.level] >= this.levels[level]) {
            const args = Array.from(arguments);
            args.unshift(level);
            this._log.apply(this, args);
        }
    }
}

module.exports = new Logger(config.log);
