require('events').EventEmitter.prototype._maxListeners = 0;

const crypto = require('crypto');
const io = require('socket.io-emitter');
const kue = require('kue');
const LndGrpc = require('@dmidnight/lnd-grpc');
const mongoose = require('mongoose');
const Redis = require('ioredis');
const log = require('./log');

const Card = require('../models/card');
const Payee = require('../models/payee');
const Invoice = require('../models/invoice');

const ObjectId = mongoose.Types.ObjectId;

class Worker {
    constructor(options) {
        this.lnd = {
            host: `${options.lnd.host}:${options.lnd.grpcPort}`,
            cert: `${options.lnd.dir}/tls.cert`,
            macaroon: `${options.lnd.dir}/data/chain/bitcoin/mainnet/admin.macaroon`,
            waitForCert: true,
            waitForMacaroon: true,
            walletPassword: options.lnd.walletPassword,
            version: options.lnd.version
        };
        this.mongo = options.mongo;
        this.redis = options.redis;
        this.socketio = options.socketio;
        this.state = 'stopped';

        this.publisher = null;
        this.lndgrpc = null;
    }

    async start() {
        this.state = 'starting';

        await this.connectMongo();
        await this.connectRedis();
        await this.connectLND();
        await this.connectKue();

        this.state = 'started';
    }

    async connectMongo() {
        await mongoose.connect(this.mongo.uri, {
            socketTimeoutMS: 0,
            keepAlive: true,
            reconnectTries: 30,
            useCreateIndex: true,
            useNewUrlParser: true,
            useFindAndModify: false
        });
    }

    async connectRedis() {
        this.publisher = new Redis(this.redis);
        this.io = io(this.publisher);
        this.publisher.once('connect', () => {
            log.debug('Redis publisher connected');
        });
    }

    async connectLND() {
        this.lndgrpc = new LndGrpc(this.lnd);
        this.lndgrpc.on('locked', () => log.info('LND wallet locked'));
        this.lndgrpc.on('active', () => log.info('LND wallet unlocked'));
        this.lndgrpc.on('disconnected', () => log.info('LND disconnected'));
        await this.lndgrpc.connect();
        const {WalletUnlocker} = this.lndgrpc.services;
        if (this.lndgrpc.state === 'locked') {
            await WalletUnlocker.unlockWallet({
                wallet_password: Buffer.from(this.lnd.walletPassword),
            });
        }
    }

    async connectKue() {
        this.queue = new kue.createQueue({prefix: 'kue:', redis: this.redis});
        this.queue.watchStuckJobs(1000);
        this.queue.on('error', function (err) {
            log.error('Work queue error', this.redis, err);
        });
        this.queue.process('cancelInvoice', 100, async (job, done) => {
            await this.cancelInvoiceJob(job, done);
        });
        this.queue.process('createInvoice', 100, async (job, done) => {
            await this.createInvoiceJob(job, done);
        });
        this.queue.process('rejectInvoice', 100, async (job, done) => {
            await this.rejectInvoiceJob(job, done);
        });
        this.queue.process('watchInvoice', 100, async (job, done) => {
            await this.watchInvoiceJob(job, done);
        });
    }

    async cancelInvoiceJob(job, done) {
        const {Invoices} = this.lndgrpc.services;
        const params = job.data;

        const {
            id,
            created_by
        } = params;

        let error;
        let invoice;
        let response;

        try {
            invoice = await Invoice.findOne({_id: id, created_by}).exec();
            const cancelRes = await Invoices.cancelInvoice({
                payment_hash: invoice.r_hash,
            });
            log.info(`Cancel invoice response: ${JSON.stringify(cancelRes)}`);
        } catch (e) {
            log.error(e);
            error = e;
        }

        done(error, response);
    }

    async createInvoiceJob(job, done) {
        const {Invoices} = this.lndgrpc.services;
        const params = job.data;
        const invoice_id = new ObjectId();

        const {
            card_number,
            cancelable_seconds,
            created_at,
            created_by,
            description,
            expires_at,
            metadata,
            order_id,
            payable_seconds,
            sats,
            quoted_amount,
            quoted_currency,
            quoted_rate
        } = params;

        const createdAtDate = new Date(created_at);
        const now = new Date();
        const secondsElapsed = Math.ceil((now - createdAtDate) / 1000);

        let error;
        let response;
        let invoice;
        let lnInvoice;
        try {
            const card = await Card.findOne({
                number: card_number
            }).exec();
            if (!card) {
                throw new Error(`Card number does not exist: ${card_number}`);
            }
            const entity = await Payee.findOne({
                _id: created_by
            }).exec();
            if (!entity) {
                throw new Error(`Payee does not exist: ${created_by}`);
            }
            const identity = card.identity;
            const preimage = crypto.randomBytes(32);
            const r_preimage = preimage.toString('base64');
            const hash = crypto.createHash('sha256');
            hash.update(preimage);
            const r_hash = hash.digest('base64');
            let memo = entity.name;
            if (order_id) {
                memo += ` Order ${order_id}`;
            }
            const lnInvoiceData = {
                value: sats,
                expiry: payable_seconds - secondsElapsed,
                memo,
                hash: r_hash
            };
            log.debug('Adding LND Invoice', lnInvoiceData);
            lnInvoice = await Invoices.addHoldInvoice(lnInvoiceData);
            log.debug('LND AddHoldInvoice Response:', lnInvoice);
            invoice = new Invoice({
                _id: invoice_id,
                card_number,
                cancelable_seconds,
                created_at,
                created_by,
                description,
                expires_at,
                lnd: {
                    host: this.lnd.host,
                    invoice_state: 'open'
                },
                metadata,
                order_id,
                satoshi: sats,
                payable_seconds,
                payment_request: lnInvoice.payment_request,
                identity,
                quoted_amount,
                quoted_currency,
                quoted_rate,
                r_hash,
                r_preimage
            });
            await invoice.save();
            response = invoice.toJSON({virtuals: true});
            delete response.lnd;
            delete response.r_preimage;
            response.created_by = {
                name: entity.name,
                description: entity.description
            };

            const room = card_number;
            await this.io.to(room).emit('invoice-event', response);
        } catch (e) {
            log.error('Create invoice error', e);
            error = e;
        }

        done(error, response);
    }

    async rejectInvoiceJob(job, done) {
        const {Invoices} = this.lndgrpc.services;
        const params = job.data;

        const {
            id,
            identity
        } = params;

        let error;
        let invoice;
        let response;

        try {
            invoice = await Invoice.findOneAndUpdate(
                {_id: id, identity},
                {rejected_at: new Date()},
                {new: true, upsert: false}
            ).exec();
            response = await Invoices.cancelInvoice({
                payment_hash: invoice.r_hash,
            });
            log.debug(`Reject invoice response: ${JSON.stringify(response)}`);
        } catch (e) {
            log.error('Reject invoice error:', e);
            error = e;
        }

        done(error, response);
    }

    async watchInvoiceJob(job, done) {
        const {Invoices} = this.lndgrpc.services;
        const params = job.data;

        const {
            r_hash,
            cancelable_seconds
        } = params;

        let keepWatching = true;
        let invoice;

        log.debug(`Watching invoice hash: ${r_hash} for ${cancelable_seconds} seconds`);

        const lndInvoiceEvents = Invoices.subscribeSingleInvoice({r_hash});
        lndInvoiceEvents.on('data', async (lnInvoice) => {
            log.debug('LND invoice event', JSON.stringify(lnInvoice));

            const update = {
                'lnd.invoice_state': lnInvoice.state.toLowerCase(),
                satoshi_received: lnInvoice.amt_paid_sat,
                msatoshi_received: lnInvoice.amt_paid_msat
            };

            if (lnInvoice.state === 'ACCEPTED') {
                update.paid_at === new Date();
            }

            if (lnInvoice.state === 'SETTLED') {
                update.settled_at === new Date();
            }

            if (lnInvoice.state === 'CANCELED') {
                invoice.canceled_at = new Date();
            }

            invoice = await Invoice.findOneAndUpdate(
                {
                    payment_request: lnInvoice.payment_request
                },
                update,
                {new: true, upsert: false}
            ).populate('created_by').exec();

            if (invoice) {
                const room = invoice.card_number;
                const response = invoice.toJSON({virtuals: true});
                delete response.lnd;
                delete response.r_preimage;

                try {
                    await this.io.to(room).emit('invoice-event', response);
                } catch (e) {
                    log.error(`Could not send socket.io message to room: ${room}`, e);
                }
            } else {
                log.error(`Got invoice event for invoice missing from db: payment_request: ${lnInvoice.payment_request}`);
            }
        });
        lndInvoiceEvents.on('end', () => {
            log.debug(`Readable stream end for invoice ${r_hash}`);
        });
        lndInvoiceEvents.on('close', () => {
            keepWatching = false;
            log.debug(`Readable stream closed for invoice ${r_hash}`);
            done();
        });
        lndInvoiceEvents.on('error', (e) => {
            log.error(`Readable stream error for invoice ${r_hash}`, e);
            keepWatching = false;
            lndInvoiceEvents.destroy();
        });

        setTimeout(async () => {
            if (invoice.lnd.invoice_state === 'accepted') {
                try {
                    const settleRes = await Invoices.settleInvoice({preimage: invoice.r_preimage});
                    log.info(`Settle invoice response: ${JSON.stringify(settleRes)}`);
                } catch (e) {
                    log.error('Could not settle invoice', e);
                }
            } else if (invoice.lnd.invoice_state !== 'canceled') {
                try {
                    const cancelRes = await Invoices.cancelInvoice({
                        payment_hash: invoice.r_hash,
                    });
                    log.info(`Cancel invoice response: ${JSON.stringify(cancelRes)}`);
                } catch (e) {
                    log.error('Could not cancel invoice', e);
                }
            }
        }, cancelable_seconds * 1000);

        setTimeout(async () => {
            if (keepWatching) {
                lndInvoiceEvents.destroy();
            }
        }, (cancelable_seconds + 30) * 1000);
    }

    async stop() {
        this.state = 'stopping';

        await new Promise((resolve, reject) => {
            this.queue.shutdown( 5000, function (err) {
                if (err) {
                    return reject(err);
                }
                resolve();
            });
        });

        await this.lndgrpc.disconnect();

        if (this.publisher) {
            await this.publisher.quit();
        }

        await mongoose.disconnect();
        this.state = 'stopped';
    }
}

module.exports = Worker;
