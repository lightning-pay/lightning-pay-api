const Blipp = require('blipp');
const http = require('http');
const Hapi = require('@hapi/hapi');
const HapiAuthBearer = require('hapi-auth-bearer-token');
const HapiSwagger = require('hapi-swagger');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');

const HapiKue = require('../plugins/hapi-kue');
const HapiMongoose = require('../plugins/hapi-mongoose');
const HapiRedis = require('../plugins/hapi-redis');
const HapiRateLimitor = require('hapi-rate-limitor');
const HapiSocketIO = require('../plugins/hapi-socket.io');

const bearerAuthHandler = require('./bearer-auth-handler');
const routes = require('./routes');
const routeValidateHandler = require('./route-validate-handler');

class HapiService {
    constructor(options) {
        this.cookies = options.cookies;
        this.lnd = options.lnd;
        this.mongo = options.mongo;
        this.redis = options.redis;
        this.server = new Hapi.Server({
            address: '0.0.0.0',
            autoListen: true,
            debug: {
                request: ['error']
            },
            listener: http.createServer(),
            port: options.port,
            host: options.externalHostname,
            routes: {
                cors: {
                    origin: options.cors.origin,
                    credentials: true
                },
                validate: routeValidateHandler
            }
        });
        this.socketio = options.socketio;
        this.swagger = options.swagger;
        this.state = 'stopped';
    }

    async start() {
        this.state = 'starting';

        await this.server.register([
            HapiAuthBearer,
            Inert,
            Vision,
            {
                plugin: HapiSwagger,
                options: this.swagger
            },
            {
                plugin: HapiMongoose,
                options: this.mongo
            },
            {
                plugin: HapiRedis,
                options: this.redis
            },
            {
                plugin: HapiKue,
                options: this.redis
            },
            {
                plugin: HapiRateLimitor,
                options: {
                    redis: Object.assign({}, this.redis),
                    extensionPoint: 'onPreAuth'
                }
            },
            {
                plugin: HapiSocketIO,
                options: this.socketio
            },
            {
                plugin: Blipp,
                options: {showAuth: true, showStart: false}
            }
        ]);

        this.server.auth.strategy('bearer', 'bearer-access-token', bearerAuthHandler);

        const cookieConfig = {
            ttl: null,
            isSecure: this.cookies.isSecure,
            isHttpOnly: true,
            encoding: 'base64json',
            clearInvalid: true,
            strictHeader: true
        };

        this.server.state('identity', cookieConfig);

        this.server.route(routes);

        await this.server.start();
        this.state = 'started';
    }

    async stop() {
        this.state = 'stopping';
        await this.server.stop();
        this.state = 'stopped';
    }
}

module.exports = HapiService;
