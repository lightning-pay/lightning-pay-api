const big = require('big.js');
const config = require('config');
const log = require('./log');
const request = require('request-promise-native');

const BTC_SAT_RATIO = big('100000000');
// const CACHE_TTL      = config.rates.cacheTTL;

const enc = encodeURIComponent;

const getRate = async (currency) => {

    const requestObject = {
        method: 'GET',
        url: `https://apiv2.bitcoinaverage.com/indices/global/ticker/short?crypto=BTC&fiat=${enc(currency)}`,
        json: true
    };

    const proxy = config.get('rates.proxy');
    if (proxy) {
        requestObject.proxy = proxy;
    }

    try {
        const res = await request(requestObject);
        return res[`BTC${currency}`].last;
    } catch (e) {
        log.error(e);
        if (e.status == 404) {
            throw new Error(`Unknown currency: ${currency}`);
        }
        throw e;
    }
};

// Convert `amount` units of `currency` to satoshis
const toSat = async (currency, amount, rate) => {
    return big(amount)
        .div(rate)
        .mul(BTC_SAT_RATIO)
        .round(0, 3) // round up to nearest msatoshi
        .toFixed(0);
};


module.exports = {getRate, toSat};
