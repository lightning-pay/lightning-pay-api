const Joi = require('joi');
const config = require('config');

const controller = require('./controller');

const routes = [
    {
        method: 'GET',
        path: '/api/key-auth-challenge',
        options: {
            handler: controller.getKeyAuthChallenge,
            description: 'Provides a challenge message to the client for key-based authentication',
            tags: ['user'],
            plugins: {
                'hapi-rate-limitor': {
                    max: config.rateLimitor.auth,
                    duration: 60 * 1000, // per minute
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/key-auth-signature',
        options: {
            handler: controller.receiveKeyAuthSignature,
            description: 'Client provides a cryptographically signed challenge message to begin using it',
            tags: ['user'],
            validate: {
                payload: Joi.object({
                    message: Joi.string().description('The original challenge message from GET /api/key-auth-challenge'),
                    signature: Joi.string().description('The signed message')
                }).label('SignedKeyChallenge')
            },
            plugins: {
                'hapi-rate-limitor': {
                    max: config.rateLimitor.auth,
                    duration: 60 * 1000, // per minute
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/username-availability/{username}',
        options: {
            handler: controller.getUsernameAvailability,
            description: 'Crete new basic auth credentials',
            tags: ['user'],
            plugins: {
                'hapi-rate-limitor': {
                    max: config.rateLimitor.username,
                    duration: 60 * 1000, // per minute
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/new-basic-auth-credentials',
        options: {
            handler: controller.createBasicAuthCredentials,
            description: 'Crete new basic auth credentials',
            tags: ['user'],
            validate: {
                payload: Joi.object({
                    username: Joi.string().lowercase().trim().min(4).description('A unique username at least 4 characters long'),
                    password: Joi.string().min(8).trim().description('A strong password at least 8 characters long')
                }).label('NewBasicAuthCredentials')
            },
            plugins: {
                'hapi-rate-limitor': {
                    max: config.rateLimitor.auth,
                    duration: 60 * 1000, // per minute
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/basic-auth-credentials',
        options: {
            handler: controller.basicAuthentication,
            description: 'Authenticate with basic credentials',
            tags: ['user'],
            validate: {
                payload: Joi.object({
                    username: Joi.string().description('An existing username'),
                    password: Joi.string().description('A matching password')
                }).label('BasicAuthCredentials')
            },
            plugins: {
                'hapi-rate-limitor': {
                    max: config.rateLimitor.auth,
                    duration: 60 * 1000, // per minute
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/logout',
        options: {
            handler: controller.unAuthenticate,
            description: 'Log out',
            tags: ['user']
        }
    },
    {
        method: 'POST',
        path: '/api/card-request',
        options: {
            handler: controller.createCardInstance,
            description: 'Provides a virtual card to the client',
            tags: ['user'],
            plugins: {
                'hapi-rate-limitor': {
                    max: config.rateLimitor.card,
                    duration: 60 * 1000, // per minute
                }
            },
            validate: {
                payload: Joi.object({
                    expiration_seconds: Joi.number().positive().integer().min(300).max(31557600).description('Provide a number of seconds to listen for events')
                }).label('CardRequest')
            }
        }
    },
    {
        method: 'GET',
        path: '/api/payments',
        options: {
            handler: controller.getInvoices,
            description: 'Get the history of payments for this identity',
            tags: ['user'],
            validate: {
                query: Joi.object({
                    skip: Joi.number().optional().integer().greater(-1).description('Number of record to skip (for pagination)'),
                    limit: Joi.number().optional().integer().positive().max(100).description('Limit number of records returned per query')
                })
            }
        }
    },
    {
        method: 'POST',
        path: '/api/payment-request/{id}/rejection',
        options: {
            handler: controller.rejectInvoice,
            description: 'Reject a payment request sent to you',
            tags: ['user']
        }
    },
    // vendor API routes
    {
        method: 'POST',
        path: '/api/payment-request/{card_number}',
        options: {
            auth: 'bearer',
            handler: controller.createInvoice,
            description: 'Create a payment request',
            tags: ['api', 'merchant'],
            validate: {
                payload: Joi.object({
                    amount: Joi.number().required().positive().description('The amount to approve in the provided currency'),
                    currency: Joi.string().required().valid(Object.keys(config.currencies)).description('The currency code to use for rate conversion'),
                    description: Joi.string().description('The merchants order description'),
                    metadata: Joi.string().description('Provide any useful metadata to store with this record'),
                    order_id: Joi.string().description('Provide an optional order ID'),
                    payable_seconds: Joi.number().positive().integer().min(30).max(60 * 60 * 24 * 7).description('Provide a number of seconds to allow payment'),
                    cancelable_seconds: Joi.number().positive().integer().min(30).max(60 * 60 * 24 * 7).description('Provide a number of seconds to allow cancellation'),
                }).label('PaymentRequest')
            }
        }
    },
    {
        method: 'GET',
        path: '/api/payment-request/{id}',
        options: {
            auth: 'bearer',
            handler: controller.getInvoice,
            description: 'Get the status of a payment request',
            tags: ['api', 'merchant']
        }
    },
    {
        method: 'DELETE',
        path: '/api/payment-request/{id}',
        options: {
            auth: 'bearer',
            handler: controller.cancelInvoice,
            description: 'Cancel a payment-request',
            tags: ['api', 'merchant']
        }
    },
];

module.exports = routes;
