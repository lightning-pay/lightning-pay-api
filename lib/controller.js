const Card = require('../models/card');
const Challenge = require('../models/challenge');
const Invoice = require('../models/invoice');
const Payer = require('../models/payer');

const {createHash} = require('crypto');
const {getRate, toSat} = require('./exchange-rate');
const secp256k1 = require('secp256k1');
const zbase32 = require('zbase32');

const log = require('./log');

const LN_MSG_PREFIX = 'Lightning Signed Message:';
const sha256 = (msg) => createHash('sha256').update(msg).digest();
const doubleSha256 = (msg) => sha256(sha256(msg));

const MSG_PREFIX = 'Sign this message to authenticate. ';

function _secondsFromNow(seconds = 120) {
    const t = new Date();
    t.setSeconds(t.getSeconds() + seconds);
    return t;
}

function _getPubkeyFromSignature(msg, sigZbase32) {
    const digest = doubleSha256(LN_MSG_PREFIX + msg);
    const sigBuffer = Buffer.from(zbase32.decode(sigZbase32));
    const r = (sigBuffer[0] - 27) & ~4;
    const sv = sigBuffer.slice(1);
    return secp256k1.recover(digest, sv, r);
}

function _cancelInvoiceJob(workQueue, data) {
    return new Promise((resolve, reject) => {
        const job = workQueue.create('cancelInvoice', data)
            .priority('high')
            .removeOnComplete( true )
            .ttl(5000) //milliseconds
            .save(function (err) {
                if (err) {
                    log.error('Error saving job', err);
                    return reject(err);
                }
                resolve(job);
            });

    });
}

function _createInvoiceJob(workQueue, data) {
    return new Promise((resolve, reject) => {
        const job = workQueue.create('createInvoice', data)
            .priority('high')
            .removeOnComplete( true )
            .ttl(5000) //milliseconds
            .save(function (err) {
                if (err) {
                    log.error('Error saving job', err);
                    return reject(err);
                }
                resolve(job);
            });

    });
}

function _rejectInvoiceJob(workQueue, identity, id) {
    return new Promise((resolve, reject) => {
        const job = workQueue.create('rejectInvoice', {
            id,
            identity
        })
            .priority('high')
            .removeOnComplete( true )
            .ttl(5000) //milliseconds
            .save(function (err) {
                if (err) {
                    log.error('Error saving job', err);
                    return reject(err);
                }
                resolve(job);
            });
    });
}

function _getJobResult(job) {
    return new Promise((resolve, reject) => {
        job
            .on('complete', (data) => {
                resolve(data);
            })
            .on('failed attempt', function (errorMessage, doneAttempts) {
                log.error('Job attempt failed', doneAttempts, errorMessage);
            })
            .on('failed', reject);
    });
}

function _createInvoiceWatcherJob(workQueue, invoice) {
    return new Promise((resolve, reject) => {
        workQueue.create('watchInvoice', {
            r_hash: invoice.r_hash,
            cancelable_seconds: invoice.cancelable_seconds
        })
            .priority('high')
            .removeOnComplete( true )
            .save(function (err) {
                if (err) {
                    log.error('Error saving job', err);
                    return reject(err);
                }
                return resolve();
            });
    });

}

// User routes

async function getKeyAuthChallenge(req, h) {
    let challenge;

    try {
        challenge = new Challenge();
        await challenge.save();
    } catch (e) {
        log.error('Error creating key auth challenge', e);
        return h.response({error: 'Could not create auth challenge. Please try again'}).unstate('identity').code(500);
    }

    const response = {
        message: `${MSG_PREFIX}${challenge._id}`
    };

    return h.response(response).unstate('identity').code(201);
}

async function receiveKeyAuthSignature(req, h) {
    let challenge;
    const message = req.payload.message;
    const signature = req.payload.signature;

    let pubkey;

    try {
        pubkey = _getPubkeyFromSignature(message, signature).toString('hex');

        const token = message.replace(MSG_PREFIX, '');

        challenge = await Challenge.findOneAndUpdate(
            {
                _id: token,
                burned: false
            },
            {
                burned: true
            }, {
                new: true,
                upsert: false
            }).exec();
        if (!challenge) {
            return h.response({error: 'Challenge not found'}).unstate('identity').code(403);
        }
    } catch (e) {
        log.error('Error parsing challenge signature', e);
        return h.response({error: 'Error parsing challenge signature'}).unstate('identity').code(403);
    }

    const response = {
        id: pubkey
    };

    return h.response(response).state('identity', response).code(200);
}

async function getUsernameAvailability(req, h) {
    const username = req.params.username;
    let found;
    try {
        found = await Payer.findOne({username}).exec();
    } catch (e) {
        return h.response({error: 'Temporary system error. Please try your request again.'}).code(500);
    }
    if (found) {
        return h.response({username, available: false}).code(200);
    }
    return h.response({username, available: true}).code(200);
}

async function createBasicAuthCredentials(req, h) {
    const username = req.payload.username;
    const password = req.payload.password;
    let payer;
    try {
        payer = new Payer({username});
        payer.setPassword(password);
        await payer.save();
    } catch (e) {
        log.error(`Could not create new payer: ${e.message}`);
        return h.response({error: 'Could not create user. Please try again'}).unstate('identity').code(500);
    }

    const response = {
        id: payer._id.toString()
    };

    return h.response({response}).state('identity', response).code(201);
}

async function basicAuthentication(req, h) {
    const username = req.payload.username;
    const password = req.payload.password;
    let payer;
    try {
        payer = await Payer.findOne({username}).exec();
        if (payer && payer.testPassword(password)) {
            payer.last_login = new Date();
            await payer.save();
        } else {
            throw new Error('Invalid login attempt');
        }
    } catch (e) {
        return h.response({error: 'Could not authenticate user. Please try again'}).unstate('identity').code(403);
    }

    const response = {
        id: payer._id.toString()
    };

    return h.response({response}).state('identity', response).code(200);
}

async function createCardInstance(req, h) {
    const expirationSeconds = req.payload.expiration_seconds;
    const expires = _secondsFromNow(expirationSeconds);

    const identity = req.state && req.state.identity && req.state.identity.id;
    if (!identity) {
        return h.response({error: 'Not authenticated'}).code(403);
    }
    const card = new Card({
        expires_at: expires,
        identity
    });

    try {
        await card.save();
    } catch (e) {
        log.error(`Could not create card: ${e.message}`);
        return h.response({error: 'Could not create card. Please try again'}).code(500);
    }

    const response = card.toJSON();

    return h.response(response).code(201);
}

async function getInvoices(req, h) {
    const skip = req.query.skip || 0;
    const limit = req.query.limit || 20;

    try {
        const identity = req.state && req.state.identity && req.state.identity.id;
        if (!identity) {
            return h.response({error: 'Not authenticated'}).code(403);
        }

        const invoices = await Invoice.find({
            identity
        },
        {
            'lnd.host': 0
        })
            .sort({created_at: -1})
            .limit(limit)
            .skip(skip)
            .populate('created_by', 'name description')
            .exec();

        return h.response(invoices).code(200);
    } catch (e) {
        log.error(`Error fetching history: ${e.message}`);
        return h.response({error: 'Could not fetch payment history'}).code(500);
    }
}

async function rejectInvoice(req, h) {
    const id = req.params.id;

    let data;
    try {
        const identity = req.state && req.state.identity && req.state.identity.id;
        if (!identity) {
            return h.response({error: 'Not authenticated'}).code(403);
        }

        const job = await _rejectInvoiceJob(req.server.app.workQueue, identity, id);
        data = await _getJobResult(job);
    } catch (e) {
        log.debug('Reject invoice job failed', e);
        return h.response({
            error: 'Could not process rejection request'
        }).code(500);
    }

    return h.response({
        data
    }).code(200);
}

async function unAuthenticate(req, h) {
    return h.response({status: 'success'}).unstate('identity').code(200);
}

// Merchant Routes

async function createInvoice(req, h) {
    const created_by = req.auth.artifacts.id;

    const card_number = req.params.card_number;
    const quoted_amount = req.payload.amount;
    const quoted_currency = req.payload.currency;

    const description = req.payload.description;
    const metadata = req.payload.metadata;
    const order_id = req.payload.order_id;

    const payable_seconds = req.payload.payable_seconds || 30;
    const cancelable_seconds = req.payload.cancelable_seconds || 60 * 5;

    const created_at = new Date();
    const expires_at = created_at;
    expires_at.setSeconds(expires_at.getSeconds() + payable_seconds);

    let invoice;
    try {
        const quoted_rate = await getRate(quoted_currency);
        const sats = await toSat(quoted_currency, quoted_amount, quoted_rate);

        const createInvoiceJobData = {
            card_number,
            cancelable_seconds,
            created_at: created_at.valueOf(),
            created_by,
            description,
            expires_at: expires_at.valueOf(),
            metadata,
            order_id,
            payable_seconds,
            sats,
            quoted_amount,
            quoted_currency,
            quoted_rate
        };

        const job = await _createInvoiceJob(req.server.app.workQueue, createInvoiceJobData);
        invoice = await _getJobResult(job);
        await _createInvoiceWatcherJob(req.server.app.workQueue, invoice);
    } catch (e) {
        log.error('Create invoice job failed', e);
        return h.response({
            error: e.message
        }).code(500);
    }

    return h.response({
        invoice
    }).code(201);
}

async function getInvoice(req, h) {
    const created_by = req.auth.artifacts.id;
    const id = req.params.id;

    let invoice;

    try {
        invoice = await Invoice.findOne({
            _id: id,
            created_by
        }).exec();

        if (!invoice) {
            return h.response({error: `Invoice ${id} not found`}).code(404);
        }
    } catch (e) {
        log.error('Error finding invoice', e);
        return h.response({error: 'Error finding invoice'}).code(500);
    }

    const response = invoice.toJSON({virtuals: true});
    delete response.lnd;
    delete response._id;

    return h.response(response).code(200);
}

async function cancelInvoice(req, h) {
    const created_by = req.auth.artifacts.id;
    const id = req.params.id;

    let data;
    try {
        const canelInvoiceJobData = {
            id,
            created_by
        };

        const job = await _cancelInvoiceJob(req.server.app.workQueue, canelInvoiceJobData);
        data = await _getJobResult(job);
    } catch (e) {
        log.error('Cancel invoice job failed', e);
        return h.response({
            error: e.message
        }).code(500);
    }

    return h.response({
        data
    }).code(200);
}

module.exports = {
    MSG_PREFIX,
    _createInvoiceWatcherJob,
    _createInvoiceJob,
    _getJobResult,
    _getPubkeyFromSignature,
    _secondsFromNow,
    basicAuthentication,
    cancelInvoice,
    createBasicAuthCredentials,
    createCardInstance,
    createInvoice,
    getInvoice,
    getInvoices,
    getKeyAuthChallenge,
    getUsernameAvailability,
    receiveKeyAuthSignature,
    rejectInvoice,
    unAuthenticate
};
