const Boom = require('@hapi/boom');
const log = require('./log');

module.exports = {
    failAction: async (request, h, err) => {
        if (process.env.NODE_ENV === 'production') {
        // In prod, log a limited error message and throw the default Bad Request error.
            log.error('ValidationError:', err.message);
            throw Boom.badRequest('Invalid request payload input');
        } else {
        // During development, log and respond with the full error.
            log.error(err);
            throw err;
        }
    }
};