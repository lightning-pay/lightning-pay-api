const chai = require('chai');
const request = require('supertest');
const sinon = require('sinon');
chai.should();

const config = require('config');

const kue = require('kue');
const KueMock = require('../util/kue-mock');
const mockqueue = new KueMock(kue, {redis: config.redis});

const HapiKue = require('../../plugins/hapi-kue');
const HapiMongoose = require('../../plugins/hapi-mongoose');
const HapiRedis = require('../../plugins/hapi-redis');
const HapiRateLimitor = require('hapi-rate-limitor');
const HapiSocketIO = require('../../plugins/hapi-socket.io');

const Card = require('../../models/card');
const Challenge = require('../../models/challenge');
const Invoice = require('../../models/invoice');
const Payer = require('../../models/payer');

const controller = require('../../lib/controller');
const log = require('../../lib/log');

const HapiService = require('../../lib/service');

const hapiService = new HapiService({
    cookies: config.get('cookies'),
    cors: config.get('cors'),
    externalHostname: 'localhost',
    lnd: config.get('lnd'),
    mongo: config.get('mongo'),
    port: config.get('port'),
    redis: config.get('redis'),
    socketio: config.get('socketio'),
    swagger: config.get('swagger')
});

// signMessage func
const secp256k1 = require('secp256k1');
const zbase32 = require('zbase32');
const {createHash, randomBytes} = require('crypto');
const LN_MSG_PREFIX = 'Lightning Signed Message:';
const sha256 = (msg) => createHash('sha256').update(msg).digest();
const doubleSha256 = (msg) => sha256(sha256(msg));
const signMessage = (msg, prvkey) => {
    const digest = doubleSha256(LN_MSG_PREFIX + msg);
    const sigObj = secp256k1.sign(digest, prvkey);
    const sigBuffer = Buffer.alloc(65);
    sigBuffer[0] = sigObj.recovery + 27;
    sigObj.signature.copy(sigBuffer, 1);
    return zbase32.encode(sigBuffer);
};

const generatePrvkey = () => {
    let prvkey;
    do {
        prvkey = randomBytes(32);
    } while (!secp256k1.privateKeyVerify(prvkey));
    return prvkey;
};

const stubs = {};

const fakePlugin = (server) => {
    server.ext({
        type: 'onPreStart',
        method: () => {
            return Promise.resolve();
        }
    });

    server.ext({
        type: 'onPostStop',
        method: () => {
            return Promise.resolve();
        }
    });
};

describe('Lightning Pay API', function () {

    before(async function () {
        stubs.HapiKue = sinon.stub(HapiKue.plugin, 'register').resolves(fakePlugin);
        stubs.HapiMongoose = sinon.stub(HapiMongoose.plugin, 'register').resolves(fakePlugin);
        stubs.HapiRedis = sinon.stub(HapiRedis.plugin, 'register').resolves(fakePlugin);
        stubs.HapiRateLimitor = sinon.stub(HapiRateLimitor.plugin, 'register').resolves(fakePlugin);
        stubs.HapiSocketIO = sinon.stub(HapiSocketIO.plugin, 'register').resolves(fakePlugin);

        await hapiService.start();
        (hapiService.state).should.equal('started');
    });

    after(async function () {
        await hapiService.stop();

        stubs.HapiKue.restore();
        stubs.HapiMongoose.restore();
        stubs.HapiRedis.restore();
        stubs.HapiRateLimitor.restore();
        stubs.HapiSocketIO.restore();

        (hapiService.state).should.equal('stopped');
    });

    describe('Hapi Base Service', function () {

        it('should provide a swagger.json API definition', async function () {
            return request(hapiService.server.listener)
                .get('/swagger.json')
                .set('Accept', 'application/json')
                .expect(200)
                .then((res) => {
                    (res.body).should.exist;
                });
        });

        it('should provide an API documentation route', async function () {
            return request(hapiService.server.listener)
                .get('/documentation')
                .set('Accept', 'text/html')
                .expect(200)
                .then((res) => {
                    (res.text).should.exist;
                });
        });

    });

    describe('API', () => {

        describe('user methods', () => {
            before(() => {
                stubs.logError = sinon.stub(log, 'error').returns();
            });


            after(() => {
                stubs.logError.restore();
            });

            describe('key authentication', () => {

                describe('GET /api/key-auth-challenge', () => {
                    afterEach(() => {
                        stubs.challengeSave.restore();
                    });

                    it('should provide a challenge for key authentication', async () => {
                        stubs.challengeSave = sinon.stub(Challenge.prototype, 'save').resolves();
                        const res = await hapiService.server.inject({method: 'GET', url: '/api/key-auth-challenge'});
                        (res.statusCode).should.equal(201);
                    });

                    it('should handle database errors', async () => {
                        stubs.challengeSave = sinon.stub(Challenge.prototype, 'save').rejects();
                        const res = await hapiService.server.inject({method: 'GET', url: '/api/key-auth-challenge'});
                        (res.statusCode).should.equal(500);
                    });

                });

                describe('POST /api/key-auth-signature', () => {
                    afterEach(() => {
                        stubs.findOneAndUpdate.restore();
                    });

                    it('should authenticate on a signed key auth challenge', async () => {
                        const challenge = new Challenge({burned: true});
                        const message = `${controller.MSG_PREFIX}${challenge._id}`;
                        const signature = signMessage(message, generatePrvkey());

                        stubs.findOneAndUpdate = sinon.stub(Challenge, 'findOneAndUpdate').returns({
                            exec: () => {
                                return Promise.resolve(challenge);
                            }
                        });

                        const payload = {
                            message,
                            signature
                        };
                        const res = await hapiService.server.inject({method: 'POST', url: '/api/key-auth-signature', payload});
                        (res.statusCode).should.equal(200);
                    });

                    it('should not authenticate on a signed key auth challenge that has already been burned', async () => {
                        const challenge = new Challenge({burned: true});
                        const message = `${controller.MSG_PREFIX}${challenge._id}`;
                        const signature = signMessage(message, generatePrvkey());

                        stubs.findOneAndUpdate = sinon.stub(Challenge, 'findOneAndUpdate').returns({
                            exec: () => {
                                return Promise.resolve();
                            }
                        });

                        const payload = {
                            message,
                            signature
                        };

                        const res = await hapiService.server.inject({method: 'POST', url: '/api/key-auth-signature', payload});
                        (res.statusCode).should.equal(403);
                    });

                    it('should not authenticate on a bad signature', async () => {
                        const challenge = new Challenge({burned: true});
                        const message = `${controller.MSG_PREFIX}${challenge._id}`;
                        const signature = '123412341234123412';

                        stubs.findOneAndUpdate = sinon.stub(Challenge, 'findOneAndUpdate').returns({
                            exec: () => {
                                return Promise.resolve();
                            }
                        });

                        const payload = {
                            message,
                            signature
                        };

                        const res = await hapiService.server.inject({method: 'POST', url: '/api/key-auth-signature', payload});
                        (res.statusCode).should.equal(403);
                    });

                    it('should handle database errors', async () => {
                        const challenge = new Challenge({burned: true});
                        const message = `${controller.MSG_PREFIX}${challenge._id}`;
                        const signature = signMessage(message, generatePrvkey());

                        stubs.findOneAndUpdate = sinon.stub(Challenge, 'findOneAndUpdate').returns({
                            exec: () => {
                                return Promise.reject();
                            }
                        });

                        const payload = {
                            message,
                            signature
                        };

                        const res = await hapiService.server.inject({method: 'POST', url: '/api/key-auth-signature', payload});
                        (res.statusCode).should.equal(403);
                    });
                });

            });

            describe('basic authentication', () => {

                describe('GET /api/username-availability/{username}', () => {
                    afterEach(() => {
                        stubs.payerFindOne.restore();
                    });

                    it('should return availability for unused name', async () => {
                        stubs.payerFindOne = sinon.stub(Payer, 'findOne').returns({
                            exec: () => {
                                return Promise.resolve();
                            }
                        });
                        const res = await hapiService.server.inject({method: 'GET', url: '/api/username-availability/fakeusername'});
                        (res.statusCode).should.equal(200);
                        const payload = JSON.parse(res.payload);
                        (payload.available).should.equal(true);
                    });

                    it('should return availability for used name', async () => {
                        stubs.payerFindOne = sinon.stub(Payer, 'findOne').returns({
                            exec: () => {
                                return Promise.resolve(new Payer({username: 'fakeusername'}));
                            }
                        });
                        const res = await hapiService.server.inject({method: 'GET', url: '/api/username-availability/fakeusername'});
                        (res.statusCode).should.equal(200);
                        const payload = JSON.parse(res.payload);
                        (payload.available).should.equal(false);
                    });

                    it('should handle database errors', async () => {
                        stubs.payerFindOne = sinon.stub(Payer, 'findOne').returns({
                            exec: () => {
                                return Promise.rejects(new Error('Fake error'));
                            }
                        });
                        const res = await hapiService.server.inject({method: 'GET', url: '/api/username-availability/fakeusername'});
                        (res.statusCode).should.equal(500);
                    });
                });

                describe('POST /api/new-basic-auth-credentials', () => {
                    afterEach(() => {
                        stubs.payerSave.restore();
                    });

                    it('should allow a new set of credentials to be created', async () => {
                        const payload = {
                            username: 'fakeusername',
                            password: 'badpassword'
                        };
                        stubs.payerSave = sinon.stub(Payer.prototype, 'save').resolves();
                        const res = await hapiService.server.inject({method: 'POST', url: '/api/new-basic-auth-credentials', payload});
                        (res.statusCode).should.equal(201);
                    });

                    it('should handle database errors', async () => {
                        const payload = {
                            username: 'fakeusername',
                            password: 'badpassword'
                        };
                        stubs.payerSave = sinon.stub(Payer.prototype, 'save').rejects();
                        const res = await hapiService.server.inject({method: 'POST', url: '/api/new-basic-auth-credentials', payload});
                        (res.statusCode).should.equal(500);
                    });
                });

                describe('POST /api/basic-auth-credentials', () => {
                    afterEach(() => {
                        stubs.payerSave.restore();
                        stubs.payerFindOne.restore();
                    });

                    it('should provide a session cookie on auth request with valid credentials', async () => {
                        const payload = {
                            username: 'fakeusername',
                            password: 'badpassword'
                        };

                        const payer = new Payer({username: payload.username});
                        payer.setPassword(payload.password);

                        stubs.payerSave = sinon.stub(Payer.prototype, 'save').resolves();
                        stubs.payerFindOne = sinon.stub(Payer, 'findOne').returns({
                            exec: () => {
                                return Promise.resolve(payer);
                            }
                        });

                        const res = await hapiService.server.inject({method: 'POST', url: '/api/basic-auth-credentials', payload});
                        (res.statusCode).should.equal(200);
                        (res.headers['set-cookie']).should.exist;
                        (res.headers['set-cookie'][0]).should.include('identity=');
                    });

                    it('should not provide a session cookie on auth request with invalid credentials', async () => {
                        const payload = {
                            username: 'fakeusername',
                            password: 'badpassword'
                        };

                        const payer = new Payer({username: payload.username});
                        payer.setPassword('wrongpassword');

                        stubs.payerSave = sinon.stub(Payer.prototype, 'save').resolves();
                        stubs.payerFindOne = sinon.stub(Payer, 'findOne').returns({
                            exec: () => {
                                return Promise.resolve(payer);
                            }
                        });

                        const res = await hapiService.server.inject({method: 'POST', url: '/api/basic-auth-credentials', payload});
                        (res.statusCode).should.equal(403);
                        (res.headers['set-cookie']).should.exist;
                        (res.headers['set-cookie'][0]).should.include('identity=;');
                    });
                });
            });

            describe('virtual cards', () => {
                afterEach(() => {
                    stubs.cardSave.restore();
                });

                describe('POST /api/card-request', () => {
                    it('should allow authenticated users to create virtual cards', async () => {
                        const payload = {
                            expiration_seconds: 300
                        };

                        stubs.cardSave = sinon.stub(Card.prototype, 'save').resolves();
                        const res = await hapiService.server.inject({method: 'POST', url: '/api/card-request', payload, headers: {cookie: 'identity=eyJpZCI6IjVkN2ZkODlmYWJjZDRhZGFhODFhYzBmZSJ9'}});
                        (res.statusCode).should.equal(201);
                    });

                    it('should not allow unauthenticated users to create virtual cards', async () => {
                        const payload = {
                            expiration_seconds: 300
                        };

                        stubs.cardSave = sinon.stub(Card.prototype, 'save').resolves();
                        const res = await hapiService.server.inject({method: 'POST', url: '/api/card-request', payload});
                        (res.statusCode).should.equal(403);
                    });

                    it('should handle database errors', async () => {
                        const payload = {
                            expiration_seconds: 300
                        };

                        stubs.cardSave = sinon.stub(Card.prototype, 'save').rejects(new Error('fake error'));
                        const res = await hapiService.server.inject({method: 'POST', url: '/api/card-request', payload, headers: {cookie: 'identity=eyJpZCI6IjVkN2ZkODlmYWJjZDRhZGFhODFhYzBmZSJ9'}});
                        (res.statusCode).should.equal(500);
                    });
                });

            });

            describe('invoices', () => {
                describe('GET /api/payments', () => {
                    afterEach(() => {
                        stubs.invoiceFind.restore();
                    });

                    it('should allow authenticated users to get their invoices', async () => {
                        stubs.invoiceFind = sinon.stub(Invoice, 'find').returns({
                            sort: () => {
                                return {
                                    limit: () => {
                                        return {
                                            skip: () => {
                                                return {
                                                    populate: () => {
                                                        return {
                                                            exec: () => {
                                                                return  Promise.resolve([]);
                                                            }
                                                        };
                                                    }
                                                };
                                            }
                                        };
                                    }
                                };
                            }
                        });
                        const res = await hapiService.server.inject({method: 'GET', url: '/api/payments', headers: {cookie: 'identity=eyJpZCI6IjVkN2ZkODlmYWJjZDRhZGFhODFhYzBmZSJ9'}});
                        (res.statusCode).should.equal(200);
                    });

                    it('should not allow unauthenticated users to get their invoices', async () => {
                        stubs.invoiceFind = sinon.stub(Invoice, 'find').returns({
                            sort: () => {
                                return {
                                    limit: () => {
                                        return {
                                            skip: () => {
                                                return {
                                                    populate: () => {
                                                        return {
                                                            exec: () => {
                                                                return  Promise.resolve([]);
                                                            }
                                                        };
                                                    }
                                                };
                                            }
                                        };
                                    }
                                };
                            }
                        });
                        const res = await hapiService.server.inject({method: 'GET', url: '/api/payments'});
                        (res.statusCode).should.equal(403);
                    });

                    it('should handle db errors', async () => {
                        stubs.invoiceFind = sinon.stub(Invoice, 'find').returns({
                            sort: () => {
                                return {
                                    limit: () => {
                                        return {
                                            skip: () => {
                                                return {
                                                    populate: () => {
                                                        return {
                                                            exec: () => {
                                                                return  Promise.reject(new Error('fake error'));
                                                            }
                                                        };
                                                    }
                                                };
                                            }
                                        };
                                    }
                                };
                            }
                        });
                        const res = await hapiService.server.inject({method: 'GET', url: '/api/payments', headers: {cookie: 'identity=eyJpZCI6IjVkN2ZkODlmYWJjZDRhZGFhODFhYzBmZSJ9'}});
                        (res.statusCode).should.equal(500);
                    });
                });

                describe('POST /api/payment-request/{id}/rejection', () => {
                    before(() => {
                        hapiService.server.app.workQueue = new kue.createQueue();
                    });

                    beforeEach(() => {
                        return mockqueue.clean(); // for test case isolation
                    });

                    beforeEach(() => {
                        stubs.queue = mockqueue.stub('rejectInvoice');
                    });

                    afterEach(() => {
                        stubs.queue.restore(); // for test case isolation
                    });

                    it('should allow authenticated users to reject an invoice', async () => {
                        const res = await hapiService.server.inject({method: 'POST', url: '/api/payment-request/123456/rejection', headers: {cookie: 'identity=eyJpZCI6IjVkN2ZkODlmYWJjZDRhZGFhODFhYzBmZSJ9'}});
                        (res.statusCode).should.equal(200);
                    });

                    it('should not allow unauthenticated users to reject an invoice', async () => {
                        const res = await hapiService.server.inject({method: 'POST', url: '/api/payment-request/123456/rejection'});
                        (res.statusCode).should.equal(403);
                    });
                });

            });

            describe('un-authenticating', () => {
                describe('GET /api/logout', () => {
                    it('should unset the auth cookie', async () => {
                        const res = await hapiService.server.inject({method: 'GET', url: '/api/logout'});
                        (res.statusCode).should.equal(200);
                        (res.headers['set-cookie']).should.exist;
                        (res.headers['set-cookie'][0]).should.include('identity=;');
                    });
                });
            });
        });

        describe('merchant methods', () => {
            before(() => {
                stubs.logError = sinon.stub(log, 'error').returns();
            });


            after(() => {
                stubs.logError.restore();
            });

            describe('invoice management', () => {
                describe('POST /api/payment-request/{card_number}', () => {
                    before(() => {
                        hapiService.server.app.workQueue = new kue.createQueue();
                    });

                    beforeEach(() => {
                        return mockqueue.clean(); // for test case isolation
                    });

                    beforeEach(() => {
                        stubs.createInvoice = mockqueue.stub('createInvoice', (job, done) => {
                            done(null, {
                                r_hash: 'asdfsadfasdfasdfsadfas',
                                cancelable_seconds: 2
                            });
                        });
                        stubs.watchInvoice = mockqueue.stub('watchInvoice');
                    });

                    afterEach(() => {
                        stubs.createInvoice.restore();
                        stubs.watchInvoice.restore();
                    });

                    it('should allow creating payment requests with a valid bearer token', async () => {
                        const payload = {
                            amount: 1.23,
                            currency: 'USD',
                            description: 'A test order',
                            metadata: JSON.stringify({some: {extra: 'metadata'}}),
                            order_id: '12345',
                            payable_seconds: 60,
                            cancelable_seconds: 300
                        };
                        const auth = {
                            strategy: 'bearer-access-token',
                            credentials: 'fakebearertoken123',
                            artifacts: {
                                id: '123'
                            }
                        };
                        const res = await hapiService.server.inject({method: 'POST', url: '/api/payment-request/0123456789101112', payload, auth});
                        (res.statusCode).should.equal(201);
                    });
                });

                describe('GET /api/payment-request/{id}', () => {
                    afterEach(() => {
                        stubs.invoiceFindOne.restore();
                    });

                    it('should allow checking status of an invoice', async () => {
                        stubs.invoiceFindOne = sinon.stub(Invoice, 'findOne').returns({
                            exec: () => {
                                return Promise.resolve(new Invoice({
                                    cancelable_seconds: 300,
                                    card_number: '0123456789101112',
                                    created_by: '123',
                                    expires_at: new Date(),
                                    lnd: {
                                        host: 'localhost'
                                    },
                                    satoshi: 597,
                                    payable_seconds: 60,
                                    payment_request: 'sdfsasdfsdfsadfsadfasdfs',
                                    identity: '7654567',
                                    quoted_rate: 10189.10,
                                    r_hash: 'fsdfasdfasdfsadfasdfsdad',
                                    r_preimage: 'frgbrgbrbrfgbrgfbrt'
                                }));
                            }
                        });

                        const auth = {
                            strategy: 'bearer-access-token',
                            credentials: 'fakebearertoken123',
                            artifacts: {
                                id: '123'
                            }
                        };
                        const res = await hapiService.server.inject({method: 'GET', url: '/api/payment-request/0123456789101112', auth});
                        (res.statusCode).should.equal(200);
                    });

                    it('should 404 on incorrect invoice id', async () => {
                        stubs.invoiceFindOne = sinon.stub(Invoice, 'findOne').returns({
                            exec: () => {
                                return Promise.resolve();
                            }
                        });

                        const auth = {
                            strategy: 'bearer-access-token',
                            credentials: 'fakebearertoken123',
                            artifacts: {
                                id: '123'
                            }
                        };
                        const res = await hapiService.server.inject({method: 'GET', url: '/api/payment-request/0123456789101112', auth});
                        (res.statusCode).should.equal(404);
                    });

                    it('should handle db errors', async () => {
                        stubs.invoiceFindOne = sinon.stub(Invoice, 'findOne').returns({
                            exec: () => {
                                return Promise.reject(new Error('fake error'));
                            }
                        });

                        const auth = {
                            strategy: 'bearer-access-token',
                            credentials: 'fakebearertoken123',
                            artifacts: {
                                id: '123'
                            }
                        };
                        const res = await hapiService.server.inject({method: 'GET', url: '/api/payment-request/0123456789101112', auth});
                        (res.statusCode).should.equal(500);
                    });
                });

                describe('DELETE /api/payment-request/{id}', () => {
                    before(() => {
                        hapiService.server.app.workQueue = new kue.createQueue();
                    });

                    beforeEach(() => {
                        return mockqueue.clean(); // for test case isolation
                    });

                    afterEach(() => {
                        stubs.cancelInvoice.restore();
                    });

                    it('should allow cancelation requests', async () => {
                        stubs.cancelInvoice = mockqueue.stub('cancelInvoice', (job, done) => {
                            done(null, {});
                        });
                        const auth = {
                            strategy: 'bearer-access-token',
                            credentials: 'fakebearertoken123',
                            artifacts: {
                                id: '123'
                            }
                        };
                        const res = await hapiService.server.inject({method: 'DELETE', url: '/api/payment-request/0123456789101112', auth});
                        (res.statusCode).should.equal(200);
                    });

                    it('should handle errors', async () => {
                        stubs.cancelInvoice = mockqueue.stub('cancelInvoice', (job, done) => {
                            done(new Error('fake error'), {});
                        });
                        const auth = {
                            strategy: 'bearer-access-token',
                            credentials: 'fakebearertoken123',
                            artifacts: {
                                id: '123'
                            }
                        };
                        const res = await hapiService.server.inject({method: 'DELETE', url: '/api/payment-request/0123456789101112', auth});
                        (res.statusCode).should.equal(500);
                    });
                });
            });
        });
    });
});