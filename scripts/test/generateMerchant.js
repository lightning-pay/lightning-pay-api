#!/usr/bin/env node

const config = require('config');
const crypto = require('crypto');
const faker = require('faker');
const mongoose = require('mongoose');
const Payee = require('../../models/payee');

const aYearFromNow = new Date();
aYearFromNow.setFullYear(aYearFromNow.getFullYear() + 1);

(async () => {
    await mongoose.connect(config.mongo.uri, {
        socketTimeoutMS: 0,
        keepAlive: true,
        reconnectTries: 30,
        useCreateIndex: true,
        useNewUrlParser: true,
        useFindAndModify: false
    });

    const token = crypto.randomBytes(20).toString('hex');
    const name = faker.company.companyName();
    const description = faker.company.catchPhrase();

    const entity = new Payee({
        name,
        description,
        tokens: [{
            token,
            expires: aYearFromNow
        }]
    });

    console.log(entity.toJSON());
    await entity.save();

    await mongoose.disconnect();
})();

