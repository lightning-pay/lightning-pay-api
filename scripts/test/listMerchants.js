#!/usr/bin/env node

const config = require('config');
const mongoose = require('mongoose');
const Payee = require('../../models/payee');

const aYearFromNow = new Date();
aYearFromNow.setFullYear(aYearFromNow.getFullYear() + 1);

(async () => {
    await mongoose.connect(config.mongo.uri, {
        socketTimeoutMS: 0,
        keepAlive: true,
        reconnectTries: 30,
        useCreateIndex: true,
        useNewUrlParser: true,
        useFindAndModify: false
    });

    const entities = await Payee.find().exec();
    for (const entity of entities) {
        console.log(entity.toJSON());
    }

    await mongoose.disconnect();
})();

