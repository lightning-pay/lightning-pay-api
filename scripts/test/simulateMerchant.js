#!/usr/bin/env node

const request = require('request-promise-native');
const cli = require('commander');

const BEARER = process.env.BEARER || 'unknown';
const SERVER = process.env.SERVER || 'http://localhost:3001';
const PROXYPATH = process.env.PROXYPATH || '';

cli
    .version('0.0.1')
    .option('-r, --create-payment-request [appId]', 'Request payment from an appId')
    .option('-q, --get-payment-request [paymentRequestId]', 'Get payment request status')
    .option('-c, --cancel-payment-request [paymentRequestId]', 'Request an appId for notifications')
    .parse(process.argv);

const cliOptions = {
    createPaymentRequest: cli.createPaymentRequest,
    getPaymentRequest: cli.getPaymentRequest,
    cancelPaymentRequest: cli.cancelPaymentRequest
};

function createPaymentRequest(appId) {
    const paymentRequest = {
        amount: 0.01,
        currency: 'USD',
        order_id: '1234567890',
        description: 'Order placed via test script',
        metadata: JSON.stringify({some: 'metadata'}),
        payable_seconds: 30,
        cancelable_seconds: 5 * 60
    };
    const method = 'POST';
    const url = `${SERVER}${PROXYPATH}/api/payment-request/${appId}`;
    console.log(method, url);
    request({
        method,
        url,
        json: true,
        headers: {
            Authorization: `Bearer ${BEARER}`
        },
        body: paymentRequest
    })
        .then((res) => {
            console.log(res);
        })
        .catch((e) => {
            console.error(e.message);
        });
}

function getPaymentRequest(paymentRequestId) {
    const method = 'GET';
    const url = `${SERVER}${PROXYPATH}/api/payment-request/${paymentRequestId}`;
    console.log(method, url);
    request({
        method,
        url,
        json: true,
        headers: {
            Authorization: `Bearer ${BEARER}`
        }
    })
        .then((res) => {
            console.log(res);
        })
        .catch((e) => {
            console.error(e.message);
        });
}

function cancelPaymentRequest(paymentRequestId) {
    const method = 'DELETE';
    const url = `${SERVER}${PROXYPATH}/api/payment-request/${paymentRequestId}`;
    console.log(method, url);
    request({
        method,
        url,
        json: true,
        headers: {
            Authorization: `Bearer ${BEARER}`
        }
    })
        .then((res) => {
            console.log(res);
        })
        .catch((e) => {
            console.error(e.message);
        });
}

if (cliOptions.createPaymentRequest) {
    createPaymentRequest(cliOptions.createPaymentRequest);
}

if (cliOptions.getPaymentRequest) {
    getPaymentRequest(cliOptions.getPaymentRequest);
}

if (cliOptions.cancelPaymentRequest) {
    cancelPaymentRequest(cliOptions.cancelPaymentRequest);
}
