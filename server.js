const HapiService = require('./lib/service');
const config = require('config');
const log = require('./lib/log');

(async () => {
    const service = new HapiService({
        cookies: config.get('cookies'),
        cors: config.get('cors'),
        externalHostname: config.get('externalHostname'),
        lnd: config.get('lnd'),
        mongo: config.get('mongo'),
        port: config.get('port'),
        redis: config.get('redis'),
        socketio: config.get('socketio'),
        swagger: config.get('swagger')
    });

    async function start() {
        try {
            await service.start();
        } catch (err) {
            log.info(err);
            process.exit(1);
        }

        log.info();
        log.info('Server running at:', service.server.info.uri);
        log.info('Documentation at:', `${service.server.info.uri}/documentation`);
        log.info();
        log.info(service.server.plugins.blipp.text());
    }

    async function stop() {
        try {
            await service.stop();
            log.info('Server stopped.');
        } catch (err) {
            log.info(err);
            log.info('Server killed.');
            process.exit(1);
        }
    }

    process.on('SIGTERM', stop);
    process.on('SIGINT', stop);

    await start();
})();
