const pkg = require('../package.json');

const currencies = require('./static/currencies.json');
const environment = process.env.ENVIRONMENT || process.env.NODE_ENV || 'development';
const externalHostname = process.env.EXTERNAL_HOSTNAME || 'localhost';
const externalPort = parseInt(`${process.env.EXTERNAL_PORT}`) || 3001;

let swaggerHost = externalHostname;
if (externalPort !== 80) {
    swaggerHost = `${swaggerHost}:${externalPort}`;
}

let scheme = 'https';
if (environment === 'development') {
    scheme = 'http';
}

const config = {
    cookies: {
        isSecure: false
    },
    cors: {
        origin: [`http://${externalHostname}:3000`]
    },
    currencies,
    environment,
    externalHostname,
    externalPort,
    externalProtocol: scheme,
    lnd: {
        dir: process.env.LND_DIR || '/root/.lnd',
        host: process.env.LND_HOST || 'lndbtc',
        grpcPort: parseInt(`${process.env.LND_GRPC_PORT}`) || 10009,
        version: process.env.LND_VERSION || '0.7.1-beta',
        walletPassword: process.env.WALLET_PASSWORD || 'veryBadPasswordYouShouldChangeBecauseThisIsDangerous'
    },
    log: {
        level: process.env.LOG_LEVEL || 'debug',
        timestamp: !!process.env.LOG_TIMESTAMP
    },
    mongo: {
        uri: process.env.DB_CONN_STRING || `mongodb://mongo:27017/${pkg.name}-${environment}`,
    },
    port: parseInt(`${process.env.PORT}`) || 3001,
    rates: {
        cacheTTL: process.env.RATE_CACHE_TTL || 30000,
        proxy: process.env.RATES_PROXY || null
    },
    rateLimitor: {
        auth: 20,
        card: 20,
        username: 20
    },
    redis: {
        port: 6379,          // Redis port
        host: process.env.REDIS_HOST || 'redis',
        options: {
            connectTimeout: 10000,
            keepAlive: 1
        }
    },
    socketio: {
        prefix: 'socket.io'
    },
    swagger: {
        grouping: 'tags',
        host: swaggerHost,
        info: {
            title: `${pkg.description} Documentation`,
            version: pkg.version
        },
        schemes: [scheme]
    }
};

module.exports = config;
