const externalHostname = process.env.EXTERNAL_HOSTNAME || 'lightningpay.io';
const config = {
    cookies: {
        isSecure: true
    },
    cors: {
        origin: [`https://${externalHostname}`]
    },
    rateLimitor: {
        card: 6,
        auth: 6
    },
};

module.exports = config;