require('events').EventEmitter.prototype._maxListeners = 0;

const config = require('config');
const log = require('./lib/log');

const Worker = require('./lib/worker');

(async () => {
    const worker = new Worker({
        lnd: config.get('lnd'),
        mongo: config.get('mongo'),
        redis: config.get('redis'),
        socketio: config.get('socketio')
    });

    async function start() {
        try {
            await worker.start();
        } catch (err) {
            log.info(err);
            process.exit(1);
        }

        log.info('Worker started');
    }

    async function stop() {
        try {
            await worker.stop();
            log.info('Worker stopped.');
        } catch (err) {
            log.info(err);
            log.info('Worker killed.');
            process.exit(1);
        }
    }

    process.on('SIGTERM', stop);
    process.on('SIGINT', stop);

    await start();
})();
