const Redis = require('ioredis');
const log = require('../lib/log');

module.exports.plugin = {
    name: 'hapi-redis',
    version: '1.0.0',
    once: true,
    register: async (server, options) => {

        server.app.redis = {};

        server.ext({
            type: 'onPreStart',
            method: (server) => {
                server.app.redis = {
                    publisher: new Redis(options),
                    subscriber: new Redis(options)
                };
                server.app.redis.publisher.once('connect', () => {
                    log.info('Redis publisher connected');
                });
                server.app.redis.subscriber.once('connect', () => {
                    log.info('Redis subscriber connected');
                });
            }
        });

        server.ext({
            type: 'onPostStop',
            method: async (server) => {
                if (server.app.redis && server.app.redis.publisher) {
                    try {
                        await server.app.redis.publisher.quit();
                    } catch (e) {
                        log.error('Redis publisher shutdown message:', e.message);
                    }

                }
                if (server.app.redis && server.app.redis.subscriber) {
                    try {
                        await server.app.redis.subscriber.quit();
                    } catch (e) {
                        log.error('Redis subscriber shutdown message:', e.message);
                    }
                }
            }
        });
    }
};
