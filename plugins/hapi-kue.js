const kue = require('kue');
const log = require('../lib/log');

module.exports.plugin = {
    name: 'hapi-kue',
    version: '1.0.0',
    once: true,
    register: async (server, options) => {

        server.ext({
            type: 'onPreStart',
            method: (server) => {
                const queue = new kue.createQueue({prefix: 'kue:', redis: options});
                queue.watchStuckJobs(1000);
                queue.on('error', function (err) {
                    log.error(err);
                });
                server.app.workQueue = queue;
            }
        });

        server.ext({
            type: 'onPreStop',
            method: async (server) => {
                if (server.app.workQueue ) {
                    try {
                        await new Promise((resolve, reject) => {
                            server.app.workQueue.shutdown(5000, (err) => {
                                if (err) {
                                    return reject(err);
                                }
                                resolve();
                            });
                        });
                    } catch (e) {
                        log.info('Kue shutdown message:', e.message);
                    }
                }
            }
        });
    }
};
