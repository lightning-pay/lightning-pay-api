const SocketIO = require('socket.io');
const redisAdapter = require('socket.io-redis');
const log = require('../lib/log');

module.exports.plugin = {
    name: 'hapi-socket.io',
    version: '1.0.0',
    register: async (server, options) => {

        server.ext({
            type: 'onPreStart',
            method: (server) => {
                server.app.io = SocketIO(server.listener, {path: '/api/socket.io'});
                server.app.io.adapter(redisAdapter({
                    pubClient: server.app.redis.publisher,
                    subClient: server.app.redis.subscriber,
                    prefix: options.prefix
                }));

                server.app.io.on('connection', (socket) => {
                    log.debug('socket connected', socket.id);
                    socket.on('subscribe', (room) => {
                        socket.join(room);
                        socket.emit('subscribed', room);
                        log.debug('socket', socket.id, 'joined room', room);
                    });

                    socket.on('unsubscribe', (room) => {
                        socket.leave(room);
                        socket.emit('unsubscribed', room);
                        log.debug('socket', socket.id, 'left room', room);
                    });

                    socket.on('disconnect', function () {
                        log.debug('socket disconnected', socket.id);
                    });
                });
            }
        });
    }
};
