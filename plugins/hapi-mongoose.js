const mongoose = require('mongoose');

module.exports.plugin = {
    name: 'hapi-mongoose',
    version: '1.0.0',
    once: true,
    register: async (server, options) => {
        server.ext({
            type: 'onPreStart',
            method: () => {
                return mongoose.connect(options.uri, {
                    socketTimeoutMS: 0,
                    keepAlive: true,
                    reconnectTries: 30,
                    useCreateIndex: true,
                    useNewUrlParser: true,
                    useFindAndModify: false
                });
            }
        });

        server.ext({
            type: 'onPostStop',
            method: () => {
                return mongoose.disconnect();
            }
        });
    }
};
