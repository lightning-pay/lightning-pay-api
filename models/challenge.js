const crypto = require('crypto');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

function fiveMinutesFromNow() {
    const d = new Date();
    d.setMinutes(d.getMinutes() + 1);
    return d;
}

function token() {
    return crypto.randomBytes(16).toString('hex');
}

let Challenge = new Schema({
    burned: {type: Boolean, default: false},
    created_at: {type: Date, default: Date.now},
    expires_at: {type: Date, default: fiveMinutesFromNow, expires: 0},
    _id: {type: String, default: token}
}, {versionKey: false, id: false});

module.exports = Challenge = mongoose.model('Challenge', Challenge);
