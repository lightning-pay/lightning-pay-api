const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Token = new Schema({
    token: {type: String, required: true},
    type: {type: String, enum: ['Bearer'], default: 'Bearer'},
    expires: {type: Date, default: Date.now}
}, {versionKey: false, _id: false});

let Payee = new Schema({
    name: {type: String, required: true},
    description: {type: String},
    tokens: [Token],
    created_at: {type: Date, default: Date.now},
    webhook_url_pattern: {type: String}
}, {versionKey: false});

module.exports = Payee = mongoose.model('Payee', Payee);
