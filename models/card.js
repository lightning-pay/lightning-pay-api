const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const sixteenDigitFactory = () => {
    let character = 0;
    let string = '';
    const generateDigit = () => {
        const {min, max} = {min: 0, max: 9};
        return Math.floor(Math.random() * (max - min + 1) + min);
    };

    while (character < 16) {
        string += generateDigit();
        character++;
    }
    return string;
};

let Card = new Schema({
    number: {type: String, default: sixteenDigitFactory, unique: true},
    identity: {type: String}, // payer id or pubkey,
    created_at: {type: Date, default: Date.now},
    expires_at: {type: Date, expires: 0}
}, {versionKey: false});

module.exports = Card = mongoose.model('Card', Card);
