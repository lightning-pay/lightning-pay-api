const crypto = require('crypto');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Payer = new Schema({
    username: {type: String, required: true, lowercase: true, trim: true, unique: true},
    salt: {type: String},
    hash: {type: String},
    created: {type: Date, default: Date.now},
    last_login: {type: Date, default: Date.now}
});

Payer.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(32).toString('base64');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('base64');
};

Payer.methods.testPassword = function (password) {
    const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('base64');
    return this.hash === hash;
};

module.exports = Payer = mongoose.model('Payer', Payer);