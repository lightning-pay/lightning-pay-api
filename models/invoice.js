const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Refund = new Schema({
    payment_error: {type: String},
    payment_preimage: {type: String},
    payment_route: {},
    payment_hash: {type: String},
    created_at: {type: Date}
});

let Invoice = new Schema({
    cancelable_seconds: {type: Number, required: true},
    canceled_at: {type: Date},
    card_number: {type: String, required: true},
    created_at: {type: Date, required: true, default: Date.now},
    created_by: {type: String, required: true, ref: 'Payee'},
    description: {type: String},
    expires_at: {type: Date, required: true},
    lnd: {
        invoice_state: {type: String, enum: ['open', 'settled', 'canceled', 'accepted'], default: 'open'},
        host: {type: String, required: true}
    },
    refunds: [Refund],
    metadata: {type: String},
    order_id: {type: String},
    satoshi: {type: Number, required: true},
    satoshi_received: {type: Number},
    msatoshi_received: {type: Number},
    paid_at: {type: Date},
    payable_seconds: {type: Number, required: true},
    payment_request: {type: String, required: true, unique: true},
    identity: {type: String, required: true, index: true},
    quoted_amount: {type: String},
    quoted_currency: {type: String},
    quoted_rate: {type: Number, required: true},
    r_hash: {type: String, required: true, unique: true},
    r_preimage: {type: String, required: true, unique: true},
    rejected_at: {type: Date},
    settled_at: {type: Date},
}, {toJSON: {virtuals: true}});

Invoice.virtual('status').get(function () {
    if (this.refunds && this.refunds.length) {
        return 'refunded';
    }

    if (this.lnd && this.lnd.invoice_state === 'canceled' && this.rejected_at) {
        return 'rejected';
    }

    if (this.lnd && this.lnd.invoice_state === 'canceled' && this.canceled_at) {
        return 'canceled';
    }

    if (this.lnd && this.lnd.invoice_state === 'settled') {
        return 'settled';
    }

    if (this.lnd && this.lnd.invoice_state === 'accepted') {
        return 'accepted';
    }

    if (this.expires_at < new Date()) {
        return 'expired';
    }

    return 'open';
});

module.exports = Invoice = mongoose.model('Invoice', Invoice);
